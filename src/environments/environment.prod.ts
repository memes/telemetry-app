export const environment = {
  production: true,
  debug: false,
  registrationAuthToken: 'telemetryDemo',
  registrationURL: `https://registration-dot-telemetry-demo-5677f116.appspot.com/`
};
